﻿//Напишите программу, которая принимает букву от пользователя, а затем выводит ее нулевое или не нулевое значение, в зависимости от того является ли буква строчной или нет

#include "pch.h"
#include <iostream>
#include <cctype>
using namespace std;

int main()
{
	setlocale(0, "");
	char a;
	cout << "Введите букву: ";
	cin >> a;
	cout << islower(a) << endl;
	system("pause");
	return 0;
}


